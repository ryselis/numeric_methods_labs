import csv
import random
from datetime import date, datetime, time, timedelta
from functools import partial
from itertools import chain

import scipy.stats as st
from simpy import Environment

from analysis import date_is_holiday

START_DATE = date(2017, 12, 13)
SURGERY_BED_PROBABILITY = 0.84


def ts_to_time(ts: float) -> time:
    hour, remainder = divmod(ts, 3600)
    minute, second = divmod(remainder, 60)
    return time(int(hour), int(minute), int(second))


class Client:
    def __init__(self, env, scheduler, randoms, needs_beds=False):
        self._env = env
        self.action = env.process(self.run())
        self._scheduler = scheduler
        self._randoms = randoms
        self._needs_beds = needs_beds

    def run(self):
        current_day = START_DATE
        while True:
            if date_is_holiday(current_day):
                first_consultation_at = int(st.exponnorm.rvs(10.53, loc=10.12, scale=0.29) * 3600)
            else:
                first_consultation_at = int(self._randoms["registration_start_times"]() * 3600)
            try:
                t = ts_to_time(first_consultation_at)
            except ValueError:
                t = None
            if t is not None:
                total_consultation_times = 0
                if date_is_holiday(current_day):
                    if st.uniform.rvs() < self._randoms["registration_chance_on_holiday"]:
                        yield self._env.timeout(first_consultation_at)
                        self._add_consultation()
                    else:
                        yield self._env.timeout(first_consultation_at)
                else:
                    last_consultation_at = 25 * 3600
                    while last_consultation_at >= 24 * 3600 or last_consultation_at < first_consultation_at:
                        last_consultation_at = self._randoms["registration_end_times"]() * 3600
                    yield self._env.timeout(first_consultation_at)
                    self._add_consultation()
                    rvs = self._randoms["registration_count"]()
                    timeouts = [max(self._randoms["time_between_registrations"](), 0)
                                for _ in range(int(round(rvs)) - 1)]
                    coeff = sum(timeouts) / (last_consultation_at - first_consultation_at)
                    timeouts = [t / coeff for t in timeouts]
                    for i in timeouts:
                        total_consultation_times += i
                        yield self._env.timeout(i)
                        self._add_consultation()
                yield self._env.timeout(3600 * 24 - first_consultation_at - total_consultation_times)
            else:
                yield self._env.timeout(3600 * 24)
            current_day += timedelta(days=1)

    def _add_consultation(self):
        dt = datetime.combine(START_DATE, time(0, 0, 0)) + timedelta(seconds=self._env.now)
        duration = random.choice(self._randoms["durations"])
        self._scheduler.add_consultation(dt, duration, self._needs_beds)


class Scheduler:
    CONSULTATIONS_PER_DOCTOR_PER_DAY = 1
    WORK_STARTS_AT = 8 * 3600

    def __init__(self):
        self.dates = {}
        with open("time_in_hospital.csv", "r") as f:
            reader = csv.reader(f)
            lines = list(reader)
        self._times = [int(l[0]) for l in lines]

    def add_consultation(self, time_registered, duration, needs_beds):
        i = 1
        while True:
            dt = time_registered.date() + timedelta(days=i)
            if date_is_holiday(dt):
                i += 1
                continue
            if dt not in self.dates:
                self._add_day(dt)
            try:
                scheduled_at = self.dates[dt]["available_doctors"].add_visit(duration) + self.WORK_STARTS_AT
                scheduled_at_time = ts_to_time(scheduled_at)
                self.dates[dt]["registered_consultations"].append({
                    "registered_at": scheduled_at_time,
                    "duration": duration
                })
                if needs_beds and random.random() > SURGERY_BED_PROBABILITY:
                    for j in range(random.choice(self._times)):
                        bed_dt = dt + timedelta(days=0)
                        if bed_dt not in self.dates:
                            self._add_day(bed_dt)
                        self.dates[bed_dt]["beds_taken"] += 1
                break
            except ValueError:
                i += 1

    def _add_day(self, dt):
        doctors_this_day = int(st.triang.rvs(0.55, loc=13.68, scale=16.81))
        self.dates[dt] = {
            "available_doctors": DoctorCollection(doctors_this_day),
            "registered_consultations": [],
            "beds_taken": 0
        }


class DoctorCollection:
    def __init__(self, number_of_doctors):
        self._number_of_doctors = number_of_doctors
        self._doctors = [{
            "time_available": st.chi.rvs(0.8, loc=600, scale=22238.64),
            "time_taken": 0
        } for i in range(number_of_doctors)]

    def add_visit(self, duration):
        doctor_with_most_time = max(self._doctors, key=lambda doctor: self._get_doctor_available_time(doctor))
        if self._get_doctor_available_time(doctor_with_most_time) >= 0.5 * duration:
            doctor_with_most_time["time_taken"] += duration
            return doctor_with_most_time["time_taken"] - duration
        else:
            raise ValueError("Not enough time this day")

    @staticmethod
    def _get_doctor_available_time(doctor):
        return doctor["time_available"] - doctor["time_taken"]


def get_durations_from_file(fn):
    with open(fn, "r") as cdf:
        return [float(l.strip()) for l in cdf.readlines()]


if __name__ == "__main__":
    TOTAL_BEDS = 18
    queue_lengths = []
    patients_not_fit = []
    for i in range(11):
        env = Environment()
        scheduler = Scheduler()

        consultation_randoms = {
            "registration_start_times": partial(st.dgamma.rvs, 0.95, loc=9.29, scale=0.35),
            "registration_end_times": partial(st.johnsonsu.rvs, -0.94, 0.97, loc=17.39, scale=0.63),
            "registration_count": partial(st.dgamma.rvs, 1.23, loc=30.34, scale=5.81),
            "time_between_registrations": partial(st.exponweib.rvs, 1.38, 0.75, loc=-0.02, scale=735.05),
            "registration_chance_on_holiday": 37 / 263,
            "durations": get_durations_from_file("Consultation_durations.csv")
        }
        procedure_randoms = {
            "registration_start_times": partial(st.johnsonsu.rvs, 0.45, 0.85, loc=9.13, scale=0.22),
            "registration_end_times": partial(st.exponnorm.rvs, 3.14, loc=18.02, scale=0.48),
            "registration_count": partial(st.dweibull.rvs, 1.36, loc=87.4, scale=16.28),
            "time_between_registrations": partial(st.wald.rvs, loc=-62.84, scale=446.66),
            "registration_chance_on_holiday": 53 / 264,
            "durations": get_durations_from_file("Procedure_durations.csv")
        }
        surgery_randoms = {
            "registration_start_times": partial(st.johnsonsu.rvs, -0.9, 0.82, loc=9.34, scale=0.25),
            "registration_end_times": partial(st.tukeylambda.rvs, -0.29, loc=18.1, scale=0.48),
            "registration_count": partial(st.dweibull.rvs, 1.36, loc=14.51, scale=4.43),
            "time_between_registrations": partial(st.halfgennorm.rvs, 0.71, loc=2, scale=1151.02),
            "registration_chance_on_holiday": 5 / 44,
            "durations": get_durations_from_file("Surgery_durations.csv")
        }
        client = Client(env, scheduler, consultation_randoms)
        Client(env, scheduler, procedure_randoms)
        Client(env, scheduler, surgery_randoms, needs_beds=True)
        env.run(until=3600 * 24 * 365)
        last_consultation = max(scheduler.dates.keys())
        last_simulation_date = START_DATE + timedelta(days=365)
        queue_length = (last_consultation - last_simulation_date).days
        queue_lengths.append(queue_length)
        dates = sorted(scheduler.dates.keys())
        dates = [scheduler.dates[d]["beds_taken"] for d in dates]
        patients_not_fit.append(max(dates))
    print(patients_not_fit)
    print("median", sorted(queue_lengths)[len(queue_lengths) // 2 - 1])
    print("patients not fit median", sorted(patients_not_fit)[len(patients_not_fit) // 2 - 1])

    # with open("res.csv", "w") as res:
    #     res.writelines([cons.strftime("%Y-%m-%d %H:%M:%S") + "\n" for cons in client.consultations])
