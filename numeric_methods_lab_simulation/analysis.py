import warnings
from datetime import datetime, date, time

import numpy
import pandas as pandas
import scipy.stats as st
from matplotlib import pyplot
from tqdm import tqdm


def date_is_holiday(dt: date) -> bool:
    if dt.weekday() in (5, 6):
        return True
    holidays = (date(2017, 1, 1), date(2017, 2, 16), date(2017, 3, 11), date(2017, 4, 16), date(2017, 4, 17),
                date(2017, 5, 1), date(2017, 6, 24), date(2017, 7, 6), date(2017, 8, 15), date(2017, 11, 1),
                date(2017, 12, 24), date(2017, 12, 25), date(2017, 12, 26))
    return dt in holidays


def check_consultation_registration_start_times_distribution(visit_type, file_name):
    with open(file_name, "r") as fh:
        times = [datetime.strptime(dt.strip(), "%Y-%m-%d %H:%M:%S") for dt in fh.readlines()]
    series = pandas.Series(times)
    non_holiday_times = series[series.apply(lambda x: not date_is_holiday(x.date()))]
    non_holiday_times_grouped_by_date = non_holiday_times.groupby(non_holiday_times.apply(lambda x: x.date()))
    start_times = non_holiday_times_grouped_by_date.min()
    end_times = non_holiday_times_grouped_by_date.max()
    find_best_distribution_for_times(start_times,
                                     chart_title="Best distribution for %s registration start times" % visit_type)
    find_best_distribution_for_times(end_times,
                                     chart_title="Best distribution for %s registration end times" % visit_type)
    consultation_counts = non_holiday_times_grouped_by_date.count()
    find_best_distribution_for_times(consultation_counts, time_data=False,
                                     chart_title="Best distribution for %s registration count" % visit_type)
    diffs = []
    for _, group in non_holiday_times_grouped_by_date:
        diffs.extend((group - group.shift())[1:])
    diffs = pandas.Series(diffs).apply(lambda x: x.total_seconds())
    holiday_times = series[series.apply(lambda x: date_is_holiday(x.date()))]
    holiday_times_grouped_by_date = holiday_times.groupby(holiday_times.apply(lambda x: x.date()))
    start_times = holiday_times_grouped_by_date.min()
    find_best_distribution_for_times(start_times,
                                     chart_title="Best distribution for %s registration start times during "
                                                 "weekends / holidays" % visit_type)
    consultation_counts_during_holidays = holiday_times_grouped_by_date.count()
    print(visit_type, consultation_counts_during_holidays.size, "/", series.max() - series.min())
    find_best_distribution_for_times(diffs, False,
                                     chart_title="Best distribution for time between %s registrations" % visit_type)


def find_best_distribution_for_times(times, time_data=True, chart_title=None, discrete=False):
    if time_data:
        raw_times_as_ints = times.apply(lambda x: (3600 * x.hour + 60 * x.minute + x.second) / 3600)
    else:
        raw_times_as_ints = times
    # grouped = raw_times_as_ints.groupby(pandas.Categorical(raw_times_as_ints)).count()

    pyplot.figure()
    bins = 50
    ax = raw_times_as_ints.plot(kind='hist', bins=bins, normed=True, alpha=0.5, color=pyplot.rcParams['axes.color_cycle'][1])
    best_fit_name, best_fir_paramms = best_fit_distribution(raw_times_as_ints, bins, discrete)
    best_dist = getattr(st, best_fit_name)
    pdf = make_pdf(best_dist, best_fir_paramms)
    ax = pdf.plot(lw=2, label='PDF', legend=True)
    raw_times_as_ints.plot(kind='hist', bins=bins, normed=True, alpha=0.5, label='Data', legend=True, ax=ax)
    param_names = (best_dist.shapes + ', loc, scale').split(', ') if best_dist.shapes else ['loc', 'scale']
    param_str = ', '.join(['{}={:0.2f}'.format(k, v) for k, v in zip(param_names, best_fir_paramms)])
    dist_str = '{}({})'.format(best_fit_name, param_str)
    ax.set_title("%(chart_title)s:\n%(distribution)s" % {"chart_title": chart_title, "distribution": dist_str})
    pyplot.show()


def best_fit_distribution(data, bins=200, discrete=False):
    """Model data by finding best fit distribution to data"""
    # Get histogram of original data
    y, x = numpy.histogram(data, bins=bins, density=True)
    x = (x + numpy.roll(x, -1))[:-1] / 2.0

    # Distributions to check
    DISTRIBUTIONS = [
        st.alpha,st.anglit,st.arcsine,st.beta,st.betaprime,st.bradford,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,
        st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm,st.exponweib,st.exponpow,st.f,st.fatiguelife,st.fisk,
        st.foldcauchy,st.foldnorm,st.frechet_r,st.frechet_l,st.genlogistic,st.genpareto,st.gennorm,st.genexpon,
        st.genextreme,st.gausshyper,st.gamma,st.gengamma,st.genhalflogistic,st.gilbrat,st.gompertz,st.gumbel_r,
        st.gumbel_l,st.halfcauchy,st.halflogistic,st.halfnorm,st.halfgennorm,st.hypsecant,st.invgamma,st.invgauss,
        st.invweibull,st.johnsonsb,st.johnsonsu,st.ksone,st.kstwobign,st.laplace,st.levy,st.levy_l,st.levy_stable,
        st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.lomax,st.maxwell,st.mielke,st.nakagami,st.ncx2,st.ncf,
        st.nct,st.norm,st.pareto,st.pearson3,st.powerlaw,st.powerlognorm,st.powernorm,st.rdist,st.reciprocal,
        st.rayleigh,st.rice,st.recipinvgauss,st.semicircular,st.t,st.triang,st.truncexpon,st.truncnorm,st.tukeylambda,
        st.uniform,st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max,st.wrapcauchy
    ]
    DISCRETE_DISTRIBUTIONS = [
        st.bernoulli, st.binom, st.boltzmann, st.planck, st.poisson, st.geom, st.nbinom, st.hypergeom, st.zipf,
        st.logser, st.randint, st.dlaplace
    ]

    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = numpy.inf

    # Estimate distribution parameters from data
    for distribution in tqdm(DISTRIBUTIONS if not discrete else DISCRETE_DISTRIBUTIONS, desc="Checking distributions"):

        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            with warnings.catch_warnings():
                warnings.filterwarnings('ignore')

                # fit dist to data
                params = distribution.fit(data)

                # Separate parts of parameters
                arg = params[:-2]
                loc = params[-2]
                scale = params[-1]

                # Calculate fitted PDF and error with fit in distribution
                pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                sse = numpy.sum(numpy.power(y - pdf, 2.0))

                # identify if this distribution is better
                if best_sse > sse > 0:
                    best_distribution = distribution
                    best_params = params
                    best_sse = sse

        except Exception:
            pass


    return (best_distribution.name, best_params)


def make_pdf(dist, params, size=10000):
    """Generate distributions's Propbability Distribution Function """

    # Separate parts of parameters
    arg = params[:-2]
    loc = params[-2]
    scale = params[-1]

    # Get sane start and end points of distribution
    start = dist.ppf(0.01, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.01, loc=loc, scale=scale)
    end = dist.ppf(0.99, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.99, loc=loc, scale=scale)

    # Build PDF and turn into pandas Series
    x = numpy.linspace(start, end, size)
    y = dist.pdf(x, loc=loc, scale=scale, *arg)
    pdf = pandas.Series(y, x)

    return pdf


def check_doctor_work_hours():
    with open("output.csv", "r") as fh:
        lines = fh.readlines()
    stripped_lines = [l.strip() for l in lines]
    times = sorted([{
        "date": datetime.strptime(line.split(";")[0], "%Y-%m-%d %H:%M:%S"),
        "name": line.split(";")[2],
        "end_date": datetime.strptime(line.split(";")[1], "%Y-%m-%d %H:%M:%S")
    } for line in stripped_lines if len(line.split(";")) == 3], key=lambda entry: entry["date"])
    times = [t for t in times if not date_is_holiday(t["date"].date())]
    data_frame = pandas.DataFrame(times, columns=["date", "name", "end_date"])

    times_grouped_by_date = data_frame.groupby(data_frame.apply(lambda x: x["date"].date(), axis=1))
    doctors_each_day = times_grouped_by_date["name"].unique().apply(lambda x: x.size)
    find_best_distribution_for_times(doctors_each_day, time_data=False, chart_title="Amount of doctors at work")
    find_best_distribution_for_times(times_grouped_by_date["date"].count(), time_data=False,
                                     chart_title="Visits per day")
    data_frame["duration"] = (data_frame["end_date"] - data_frame["date"]).apply(lambda x: x.total_seconds())
    times_grouped_by_date_and_doctor = data_frame.groupby([data_frame.apply(lambda x: x["date"].date(), axis=1),
                                                           "name"])
    accepting_patients_per_day = times_grouped_by_date_and_doctor["duration"].sum()
    find_best_distribution_for_times(accepting_patients_per_day, time_data=False,
                                     chart_title="Time doctor accepting patients per day")


if __name__ == "__main__":
    visit_registration_files = {
        "consultation": "consulation registration times.csv",
        "surgery": "surgery_registration_times.csv",
        "procedure": "procedure_registration_times.csv"
    }
    for visit_type, file_name in visit_registration_files.items():
        check_consultation_registration_start_times_distribution(visit_type, file_name)
    check_doctor_work_hours()
