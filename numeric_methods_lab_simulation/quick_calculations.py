import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import scipy.stats


def main():
    values_orig = [11, 11, 14, 11, 11, 11, 10, 14, 9, 12, 12]
    values_mod = [12, 15, 14, 11, 11, 14, 11, 12, 11, 14, 13]
    for values in (values_orig, values_mod):
        vals = []
        confidence_levels = (80, 90, 95)
        for confidence_interval in confidence_levels:
            vals.append(mean_confidence_interval(values, confidence_interval / 100))
        res = plt.hist(values)
        hists = res[0]
        plt.xlabel("Beds taken")
        plt.ylabel("Occurrence frequency")
        lowers = [val[1] for val in vals]
        uppers = [val[2] for val in vals]
        colors = ("red", "orange", "green")
        for l, u, col, lvl in zip(lowers, uppers, colors, confidence_levels):
            plt.vlines(x=l, ymin=min(hists), ymax=max(hists), linestyles=":", colors=col)
            plt.text(l, 2, "%s%% confidence" % lvl, rotation=90, verticalalignment='center')
            plt.vlines(x=u, ymin=min(hists), ymax=max(hists), linestyles=":", colors=col)
            plt.text(u, 2, "%s%% confidence" % lvl, rotation=90, verticalalignment='center')
        plt.show()


def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, m-h, m+h


if __name__ == '__main__':
    main()
