from function import initial_conditions, u_der, h
from terminator import can_terminate


def euler():
    x, t, u = initial_conditions["x0"], 0, initial_conditions["dx/dt0"]
    current_h, limit = 0, 100
    plot_data = {"t": [t], "x": [x]}
    while current_h < limit and not can_terminate(plot_data["x"]):
        t, u, x = euler_step(t, u, x)
        current_h += h
        plot_data["t"].append(t)
        plot_data["x"].append(x)
    return plot_data, "Euler"


def euler_step(t, u, x):
    (x,
     t,
     u,) = (x + h * u,
            t + h,
            u + h * u_der(t, x, u))
    return t, u, x
