import math


def f(t):
    return math.cos(t)


def u_der(t, x, u):
    return (f(t)-c*u-k*x) / m


m = 5
c = 1
k = 1.2
h = 0.4
epsilon = 1e-6

initial_conditions = {
    "x0": 2,
    "dx/dt0": 1
}
