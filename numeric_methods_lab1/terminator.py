EPSILON = 1e-3


def can_terminate(y_history: list) -> bool:
    reversed_history = list(reversed(y_history))
    first_maximum_index, first_maximum_point = find_local_maximum(reversed_history)
    if first_maximum_index is None:
        return False
    second_maximum_index, second_maximum_point = find_local_maximum(reversed_history[first_maximum_index:])
    if second_maximum_index is None:
        return False
    if abs(second_maximum_point - first_maximum_point) < EPSILON:
        return True
    return False


def find_local_maximum(history):
    for index, item in enumerate(history[1:-1], start=1):
        if history[index - 1] < item > history[index + 1]:
            return index, item
    return None, None
