from numpy import average

from function import initial_conditions, h, u_der
from terminator import can_terminate


def runge_kutta4():
    x, t, u = initial_conditions["x0"], 0, initial_conditions["dx/dt0"]
    current_h, limit = 0, 100
    plot_data = {"t": [t], "x": [x]}
    while current_h < limit and not can_terminate(plot_data["x"]):
        du1 = h * u_der(t, x, u)
        dx1 = h * u

        du2 = h * u_der(t+h/2, x + dx1/2, u+du1/2)
        dx2 = h*(u+du1/2)

        du3 = h*u_der(t+h/2, x+dx2/2, u+du2/2)
        dx3 = h*(u+du2/2)

        du4 = h*u_der(t+h, x+dx3, u+du3)
        dx4 = h*(u+du3)

        weights = (1, 2, 2, 1)
        du = average((du1, du2, du3, du4), weights=weights)
        dx = average((dx1, dx2, dx3, dx4), weights=weights)

        t, x, u = t + h, x + dx, u + du

        plot_data["t"].append(t)
        plot_data["x"].append(x)
        current_h += h
    return plot_data, "Runge-Kutta 4"
