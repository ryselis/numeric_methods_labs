import matplotlib.pyplot as plt
import time

from adams_bashforth import adams_bashforth
from euler import euler
from runge_kutta4 import runge_kutta4
from runge_kutta_fehlberg import runge_kutta_fehlberg

SHOW_SEPARATE_GRAPHS = False

if __name__ == '__main__':
    funcs = [euler, runge_kutta4, runge_kutta_fehlberg, adams_bashforth]
    plot_datas = []
    for func in funcs:
        t1 = time.time()
        plot_data, title = func()
        plot_datas.append({"data": plot_data, "title": title})
        t2 = time.time()
        print(title, "elapsed", t2-t1, "seconds;", "last t value", plot_data["t"][-1])
        if SHOW_SEPARATE_GRAPHS:
            plt.plot(plot_data["t"], plot_data["x"])
            plt.title(title)
            plt.show()
    if not SHOW_SEPARATE_GRAPHS:
        handles = [plt.plot(pd["data"]["t"], pd["data"]["x"], label=pd["title"]) for pd in plot_datas]
        plt.legend()
        plt.show()
