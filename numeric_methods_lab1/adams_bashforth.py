from function import initial_conditions, h, u_der
from terminator import can_terminate


def adams_bashforth():
    x, t, u = initial_conditions["x0"], 0, initial_conditions["dx/dt0"]
    current_h, limit = 0, 100
    plot_data = {"t": [t, t], "x": [x, x], "u": [u, u]}
    while current_h < limit and not can_terminate(plot_data["x"]):
        tn1, xn1, un1 = plot_data["t"][-1], plot_data["x"][-1], \
                        plot_data["u"][-1]
        tn2, xn2, un2 = plot_data["t"][-2], plot_data["x"][-2], \
                        plot_data["u"][-2]
        new_x = plot_data["x"][-1] + h * (3/2 * un1 - 1/2 * un2)
        new_u = plot_data["u"][-1] + h * (3/2 * u_der(tn1, xn1, un1) - 1/2 *
                                          u_der(tn2, xn2, un2))
        new_t = plot_data["t"][-1] + h
        plot_data["t"].append(new_t)
        plot_data["x"].append(new_x)
        plot_data["u"].append(new_u)
        current_h += h
    return plot_data, "Adams-Bashforth"
