from function import initial_conditions, u_der, epsilon, h
from terminator import can_terminate


def runge_kutta_fehlberg():
    x, t, u = initial_conditions["x0"], 0, initial_conditions["dx/dt0"]
    current_h, limit = 0, 100
    plot_data = {"t": [t], "x": [x]}
    step = h
    while current_h < limit and not can_terminate(plot_data["x"]):
        ku1 = step * u_der(t, x, u)
        k1 = step * u

        ku2 = step * (u_der(t + step/4, x+k1/4, u+ku1/4))
        k2 = step * (u + k1/4)

        ku3 = step * (u_der(t + 3/8*step, x + 3/32*k1 + 9/32*k2, u + 3/32*ku1 + 9/32*ku2))
        k3 = step * (u + 3/32*k1 + 9/32*k2)

        ku4 = step * (u_der(t + 12/13*step,
                            x + (1932*k1 - 7200*k2 + 7296*k3)/2197,
                            u + (1932*ku1 - 7200*ku2 + 7296*ku3)/2197))
        k4 = step * (u + (1932*ku1 - 7200*ku2 + 7296*ku3)/2197)

        ku5 = step * u_der(t + step,
                           x + 439/216*k1 - 8*k2 + 3680/513*k3 - 845/4104*k4,
                           u + 439/216*ku1 - 8*k2 + 3680/513*ku3 - 845/4104*ku4)
        k5 = step * (u + 439/216*ku1 - 8*k2 + 3680/513*ku3 - 845/4104*ku4)

        ku6 = step * u_der(t + step/2,
                           x - 8/27*k1 + 2*k2 - 3544/2565*k3 + 1859/4104*k4 - 11/40*k5,
                           u + 8/27*ku1 + 2*ku2 - 3544/2565*ku3 + 1859/4104*ku4 - 11/40*ku5)
        k6 = step * (u + 8/27*ku1 + 2*ku2 - 3544/2565*ku3 + 1859/4104*ku4 - 11/40*ku5)

        x1 = x + (25/216*k1 + 1408/2565*k3 + 2197/4104*k4 - k5/5)
        x2 = x + (16/135*k1 + 6656/12825*k3 + 28561/56430*k4 - 9/50*k5 + 2/55*k6)
        if abs(x2 - x1) > epsilon * 10:
            step /= 2
            continue
        elif abs(x2 - x1) < epsilon / 10:
            step *= 2
            continue
        t, x, u = t + step, \
                  x2, \
                  u + 16/135*ku1 + 6656/12825*ku3 + 28561/56430*ku4 - 9/50*ku5 + 2/55*ku6
        current_h += step
        plot_data["t"].append(t)
        plot_data["x"].append(x)
    return plot_data, "Runge-Kutta-Fehlberg"


coeffs = (
    (),
    (1/4,),
    (3/32, 9/32),
    (1932/2197, 7200/2197, 7296/2197),
    (439/216, -8, 3680/513, -845/4104),
    (-8/27, 2, 3544/2565, 1859/4104, -11/40)
)

t_coeffs = (
    0, 1/4, 3/8, 12/13, 1, 1/2
)
