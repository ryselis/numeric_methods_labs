import itertools
from matplotlib import pyplot, tri, cm

from matrices import get_distinct_corners


def plot_triangles(solution, corner_map):
    """
    Plots solution as colormap
    :param solution: a vector of temperatures
    :param corner_map:
    """
    pyplot.figure()
    corners = get_distinct_corners(corner_map)
    x = [c.x for c in corners]
    y = [c.y for c in corners]
    solution = list(itertools.chain(*solution.tolist()))
    triang = tri.Triangulation(x, y)
    refiner = tri.UniformTriRefiner(triang)
    interp = tri.LinearTriInterpolator(triang, solution)
    new, new_z = refiner.refine_field(solution, interp, subdiv=6)
    pyplot.tripcolor(new.x, new.y, new_z, cmap=cm.jet)
    pyplot.colorbar()

    pyplot.gca().set_xlim([0, 10])
    pyplot.gca().set_ylim([0, 10])
    pyplot.show()


def plot_mesh(triangles):
    pyplot.figure()
    for triangle in triangles:
        corners = [[c.x, c.y] for c in triangle.corners]
        t = pyplot.Polygon(corners, edgecolor="blue", facecolor="green", linewidth=0.25)
        pyplot.gca().add_patch(t)
    pyplot.gca().set_xlim([0, 10])
    pyplot.gca().set_ylim([0, 10])
    pyplot.show()
