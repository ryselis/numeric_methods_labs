import itertools

import numpy

from matrices import assemble_common_stiffness_matrix, get_mask_for_known_nodes, assemble_right_hand_side, \
    get_corner_map
from plots import plot_triangles
from triangle import Triangle

#define triangles
triangles = [
    Triangle(corners=[[0, 7], [2, 4], [4, 7]], volumetric_heat_source=0, heat_conductivity=54,
             convection_coefficient=20),
    Triangle(corners=[[2, 4], [4, 1], [4, 7]], volumetric_heat_source=0, heat_conductivity=54,
             convection_coefficient=0),
    Triangle(corners=[[4, 1], [6.5, 4], [4, 7]], volumetric_heat_source=160000, heat_conductivity=24,
             convection_coefficient=22),
    Triangle(corners=[[4, 1], [9, 1], [6.5, 4]], volumetric_heat_source=160000, heat_conductivity=24,
             convection_coefficient=22)
]
# define global properties
external_temperature = 300
thickness = 0.05
heat_output = 30000


def solve(triangles):
    """
    Constructs a system of equations and solves it for a given set of triangles
    :param triangles: a set of triangles
    :return: a vector of temperatures
    """
    corner_map = get_corner_map(triangles)
    stiffness_matrix = assemble_common_stiffness_matrix(triangles, corner_map)  # K
    right_hand_side = assemble_right_hand_side(triangles, corner_map)  # whole right hand side
    mask = get_mask_for_known_nodes(corner_map)
    inverse_mask = [i for i in range(stiffness_matrix.shape[0]) if i not in mask]
    # construct matrices for known and unknown elements
    mat1 = numpy.delete(numpy.delete(stiffness_matrix, mask, 0), mask, 1)
    mat2 = numpy.matrix(numpy.delete(right_hand_side, mask, 0))
    mat3 = numpy.matmul(numpy.delete(numpy.delete(stiffness_matrix, mask, 0), inverse_mask, 1), numpy.matrix([420]))
    # solve for unknown temperatures
    res = numpy.linalg.solve(mat1, mat2 - mat3)
    # add known temperatures
    res = numpy.insert(res, 0, [420])
    return res


res = solve(triangles)
print(res.min(), res.max())
plot_triangles(res, get_corner_map(triangles))


for i in range(4):
    triangles = list(itertools.chain(*[t.segment_triangle() for t in triangles]))
    res = solve(triangles)
    print(res.min(), res.max())
    plot_triangles(res, get_corner_map(triangles))

