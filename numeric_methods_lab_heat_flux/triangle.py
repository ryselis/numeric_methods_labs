import numpy

thickness = 0.05
external_temperature = 300


class Corner:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __sub__(self, other):
        return Corner(self.x - other.x, self.y - other.y)

    def __str__(self):
        return "(%(x)s; %(y)s)" % {"x": self.x, "y": self.y}

    def __repr__(self):
        return "Corner: %s" % str(self)

    def to_numpy_matrix(self):
        return numpy.matrix([self.x, self.y])

    def to_list(self):
        return [self.x, self.y]

    def __eq__(self, other):
        return abs(self.x - other.x) < 1e-5 and abs(self.y - other.y) < 1e-5

    def __hash__(self):
        return hash((self.x, self.y))


class Triangle:
    def __init__(self, corners, volumetric_heat_source, heat_conductivity,
                 convection_coefficient):
        self.corners = [Corner(*item) for item in corners]
        self.volumetric_heat_source = volumetric_heat_source
        self.heat_conductivity = heat_conductivity
        self.convection_coefficient = convection_coefficient
        if self.matrix_n(self.average_point)[0] < 0:
            self.corners[2], self.corners[1] = self.corners[1], self.corners[2]

    def __str__(self):
        return "α=%(alpha)s; b=%(b)s; λ=%(lambda)s; [%(coords)s]" % {
            "coords": "; ".join([str(elems) for elems in self.corners]),
            "b": self.volumetric_heat_source,
            "alpha": self.convection_coefficient,
            "lambda": self.heat_conductivity
        }

    def __repr__(self):
        return "Triangle: %(str)s" % {"str": str(self)}

    @property
    def area(self) -> float:
        """
        Calculates the area of a triangle based on edge vectors cross product formula for area
        :return: area of the triangle
        """
        diff1 = (self.corners[1] - self.corners[0]).to_numpy_matrix()
        diff2 = (self.corners[2] - self.corners[0]).to_numpy_matrix()
        extended_diff1 = numpy.zeros((1, 3))
        extended_diff1[:, :-1] = diff1
        extended_diff2 = numpy.zeros((1, 3))
        extended_diff2[:, :-1] = diff2
        return numpy.linalg.norm(
            numpy.cross(extended_diff1, extended_diff2)) / 2

    @property
    def edge_lengths(self) -> list:
        """
        Gets a list of edge lengths for a triangle
        :return: a list of edge lengths
        """
        lengths = []
        for i, corner in enumerate(self.corners):
            for j, corner2 in enumerate(self.corners):
                if i > j:
                    lengths.append(numpy.linalg.norm((corner - corner2)
                                                     .to_numpy_matrix()))
        return lengths

    @property
    def stiffness_matrix(self):
        """
        Gets a stiffness matrix for a triangle based on formula K = B'DBV+αN'NS, where V - volume, S - area of
        a 3D triangle
        :return: a stiffness matrix
        :rtype: numpy.matrix
        """
        # middle point for a triangle
        b = numpy.matrix([
            self.vector_b, self.vector_c
        ])  # B matrix
        d = numpy.matrix([
            [self.heat_conductivity, 0],
            [0, self.heat_conductivity]
        ])  # D matrix
        k_e = b.transpose() * d * b * self.area * thickness + \
              self.convection_coefficient * numpy.eye(3) / 3 * self.area
        return k_e

    @property
    def average_point(self):
        average_point = [
            sum(c.x for c in self.corners) / len(self.corners),
            sum(c.y for c in self.corners) / len(self.corners)
        ]
        return average_point

    def matrix_n(self, point):
        a = self.vector_a
        b = self.vector_b
        c = self.vector_c
        return [
            a_val + b_val * point[0] + c_val * point[1]
            for a_val, b_val, c_val in zip(a, b, c)
        ]

    @property
    def vector_c(self):
        c = [
            self.corners[2].x - self.corners[1].x,
            self.corners[0].x - self.corners[2].x,
            self.corners[1].x - self.corners[0].x
        ]
        c = [item / (2 * self.area) for item in c]
        return c

    @property
    def vector_b(self):
        b = [
            self.corners[1].y - self.corners[2].y,
            self.corners[2].y - self.corners[0].y,
            self.corners[0].y - self.corners[1].y
        ]
        b = [item / (2 * self.area) for item in b]
        return b

    @property
    def vector_a(self):
        a = [
            self.corners[1].x * self.corners[2].y -
            self.corners[2].x * self.corners[1].y,

            self.corners[2].x * self.corners[0].y -
            self.corners[0].x * self.corners[2].y,

            self.corners[0].x * self.corners[1].y -
            self.corners[1].x * self.corners[0].y
        ]
        a = [item / (2 * self.area) for item in a]
        return a

    def segment_triangle(self):
        """
        Segments a triangle into 4 smaller triangles by connecting edge mid-points and creating one central
        triangle and 3 corner triangles
        """
        mid_points = {}
        for i, corner in enumerate(self.corners):
            current_mid_points = []
            for j, corner2 in enumerate(self.corners):
                if i == j:
                    continue
                lesser = min((i, j))
                greater = max((i, j))
                key = "%s-%s" % (lesser, greater)
                if key not in mid_points:
                    mid_points[key] = [
                        (corner.x + corner2.x) / 2,
                        (corner.y + corner2.y) / 2
                    ]
                current_mid_points.append(mid_points[key])
            # one corner, two mid-points
            yield Triangle([
                corner.to_list(), *current_mid_points
            ], self.volumetric_heat_source, self.heat_conductivity, self.convection_coefficient)
        # three mid-points (central triangle)
        yield Triangle(mid_points.values(), self.volumetric_heat_source, self.heat_conductivity,
                       self.convection_coefficient)

    @property
    def matrix_p(self):
        n = numpy.matrix(self.matrix_n(self.average_point))
        return n.transpose() * self.volumetric_heat_source * self.area * thickness

    @property
    def vector_s(self):
        return self.area * self.convection_coefficient * external_temperature * \
               numpy.matrix(self.matrix_n(self.average_point)).transpose()
