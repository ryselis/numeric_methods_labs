import operator

import numpy

from triangle import Corner

known_temperatures = {
    Corner(0, 7): 420
}

heat_outputs = {
    Corner(4, 1): -30000
}

external_temperature = 300


def assemble_common_stiffness_matrix(triangles, corner_map):
    """
    Assembles a stiffness matrix from all stiffness matrices of all triangles by finding the same corners and adding
    them to respective points in matrices
    :param corner_map: a dict containing corner to global index mapping
    :param triangles: a list of triangles to analyze
    :return: a common stiffness matrix for triangles
    :rtype: numpy.matrix
    """
    # stiffness matrix for each element
    stiffness_matrices = [triangle.stiffness_matrix for triangle in triangles]
    common_matrix_dimension = len(corner_map)
    # prepare global matrix
    common_matrix = numpy.zeros((common_matrix_dimension, common_matrix_dimension), float)
    for i, (triangle, stiffness_matrix) in enumerate(zip(triangles, stiffness_matrices)):
        for j, corner in enumerate(triangle.corners):
            for k, corner2 in enumerate(triangle.corners):
                # find global index and add to global matrix
                index1 = corner_map[corner]
                index2 = corner_map[corner2]
                common_matrix[index1, index2] += stiffness_matrix[j, k]
    return common_matrix


def assemble_right_hand_side(triangles, corner_map):
    """
    Assembles a common matrix for sum P+S+Q
    :param triangles: a list of triangles to analyze
    :param corner_map: a dict containing corner to global index mapping
    :return: a common P+S+Q vector
    :rtype: numpy.matrix
    """
    # get vectors P and S for each element
    p_vectors = [triangle.matrix_p for triangle in triangles]
    s_vectors = [triangle.vector_s for triangle in triangles]
    common_matrix_dimension = len(corner_map)
    # prepare common vector
    common_matrix = numpy.zeros((common_matrix_dimension, 1), float)
    for i, (triangle, p, s) in enumerate(zip(triangles, p_vectors, s_vectors)):
        for j, corner in enumerate(triangle.corners):
            # find global index and add to global vector
            index = corner_map[corner]
            f = p + s
            common_matrix[index, 0] += f[j, 0]
    for corner, output in heat_outputs.items():
        # add all heat inputs / outputs
        index = corner_map[corner]
        common_matrix[index, 0] += output
    return common_matrix


def get_mask_for_known_nodes(corner_map):
    """
    Gets a mask to filter out matrix or vector to contain only known nodes
    :param corner_map: a dict containing corner to global index mapping
    :return: a mask
    :rtype: list
    """
    return [corner_map[corner] for corner in known_temperatures.keys()]


def get_distinct_corners(corner_map):
    """
    Gets distinct corner list sorted by global index
    :param corner_map: a dict containing corner to global index mapping
    :return: a list of corners
    """
    return [c[0] for c in sorted(corner_map.items(), key=operator.itemgetter(1))]


def get_corner_map(triangles):
    """
    Finds all distinct corners and assigns them a global index. Mapping reduces list scans to find the index
    :param triangles: a list of triangles to get corner indices for
    :return: a dict containing corner to global index mapping
    """
    corners_map = {}
    for triangle in triangles:
        for corner in triangle.corners:
            if corner not in corners_map:
                # index is incremental as new corners are discovered
                corners_map[corner] = len(corners_map.keys())
    return corners_map
