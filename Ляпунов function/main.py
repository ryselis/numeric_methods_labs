import functools

import numpy
from matplotlib import pyplot, cm
from numpy import meshgrid
from scipy.integrate import odeint, ode
from mpl_toolkits.mplot3d import Axes3D

a = 0.1
b = 0.1
c = 14


def rossler(state, t):
    """
    Rossler attractor definition
    :param state: [x, y, z] tuple
    :param t: time, required for numpy
    :return: new [x, y, z] state
    """
    x, y, z = state
    return -y - z, x + a*y, b + z*(x - c)


def rossler2(t, state, override_x=None, override_y=None, override_z=None):
    """
    Rossler attractor definition for ODE solver
    :param t: time, required for numpy
    :param state: [x, y, z] list
    :param override_x: force x to given value
    :param override_y: force y to given value
    :param override_z: force z to given value
    :return:
    """
    x, y, z = state
    if override_x is not None:
        x = override_x
    if override_y is not None:
        y = override_y
    if override_z is not None:
        z = override_z
    return [-y - z, x + a*y, b + z*(x - c)]


state0 = [1, 1, 1]
t = numpy.arange(0, 100, 0.01)
# check what Rossler attractor looks like
states = odeint(rossler, state0, t)

fig = pyplot.figure()
ax = fig.gca(projection='3d')
ax.plot(states[:, 0], states[:, 1], states[:, 2])
# ----------------- vector field ----------------------------


def get_projection(projection_omitted_axis):
    """
    Draws a projection in selected plane. If projection omitted axis is x, projection is in y0z plane, etc.
    :param projection_omitted_axis: axis to omit from 3D space
    """
    pyplot.figure()
    t = numpy.arange(0, 100, 0.01)
    RANGES = [
        ("x", numpy.arange(-20, 20, 2)), ("y", numpy.arange(-50, 50, 2)),
        ("z", numpy.arange(0, 25, 2)),
    ]
    applicable_ranges = [rng[1] for rng in RANGES if rng[0] != projection_omitted_axis]
    xrange, yrange = applicable_ranges

    x1 = numpy.zeros((yrange.size, xrange.size))
    y1 = numpy.zeros((yrange.size, xrange.size))
    omitted_axis_index = {"x": 0, "y": 1, "z": 2}[projection_omitted_axis]
    for i, x in enumerate(xrange):
        for j, y in enumerate(yrange):
            rossler_params = [x, y]
            rossler_params.insert(omitted_axis_index, 0)
            xval, yval, zval = rossler(rossler_params, t)
            x1[j, i] = xval
            y1[j, i] = yval
    arrow = numpy.sqrt(numpy.square(x1) + numpy.square(y1))
    xrange_quiver, yrange_quiver = numpy.meshgrid(xrange, yrange)
    pyplot.quiver(xrange_quiver, yrange_quiver, numpy.divide(x1, arrow),
                  numpy.divide(y1, arrow), color='r')

    if projection_omitted_axis == "x":
        partial_kwarg = "override_x"
    elif projection_omitted_axis == "y":
        partial_kwarg = "override_y"
    elif projection_omitted_axis == "z":
        partial_kwarg = "override_z"
    else:
        raise ValueError("Wrong omitted axis %s" % projection_omitted_axis)
    rossler_partial = functools.partial(rossler2, **{partial_kwarg: 0})

    for x0 in numpy.arange(-20, 25, 5):
        for y0 in numpy.arange(-50, 55, 5):
            r = ode(rossler_partial).set_integrator("dopri5")\
                .set_initial_value([x0, y0, 0])
            xs = numpy.matrix(
                [r.integrate(t) for t in numpy.arange(0, 100, 0.02)]
            )
            pyplot.plot(xs[:, 0], xs[:, 1], color='b', linewidth=0.25)
    pyplot.xlim(-20, 20)
    pyplot.ylim(-50, 50)
    title = "Projection in %s plane" % "0".join([letter for letter in "xyz"
                                                 if letter != projection_omitted_axis])
    pyplot.title(title)
    pyplot.show()


get_projection("z")
get_projection("x")
get_projection("y")


# lyapunov function analysis


def get_lyapunov(projection_omitted_axis):
    """
    Draws Lyapunov function for selected plane
    :param projection_omitted_axis: axis to be removed from 3D space
    """
    x = numpy.arange(-2, 2, 0.1)
    y = numpy.arange(-2, 2, 0.1)
    x, y = meshgrid(x, y)

    omitted_axis_index = {"x": 0, "y": 1, "z": 2}[projection_omitted_axis]
    func_params = [x, y]
    func_params.insert(omitted_axis_index, 0)

    v = V(*func_params)
    py, px = numpy.gradient(v, 0.2, 0.2)
    pyplot.figure()
    pyplot.contour(x, y, v)
    pyplot.quiver(y, x, py, px)
    pyplot.title("Function V vector field when %s = 0" % projection_omitted_axis)
    pyplot.show()

    fig = pyplot.figure()
    ax = fig.gca(projection="3d")
    ax.plot_surface(x, y, v, cmap=cm.coolwarm, antialiased=True)
    ax.set_xlabel([x for x in ["x", "y", "z"] if x != projection_omitted_axis][0])
    ax.set_ylabel([x for x in ["x", "y", "z"] if x != projection_omitted_axis][1])
    ax.set_zlabel("V")
    pyplot.show()


def V(x, y, z):
    """
    Lyapunov function's for Rossler attractor definition
    """
    return x * (-201 * x / 20 - y / 2 + 625 * z / 872) + \
           y * (-x / 2 - 10 * y - 61 * z / 3912) + \
           z * (625 * x / 872 - 61 * y / 3912 - 535 * z / 34557)


get_lyapunov("z")
get_lyapunov("x")
get_lyapunov("y")
